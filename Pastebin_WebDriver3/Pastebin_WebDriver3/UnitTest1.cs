using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace GoogleCloudPricingCalculator
{
    public class GoogleCloudPlatformHomePage
    {
        private readonly IWebDriver _driver;

        public GoogleCloudPlatformHomePage(IWebDriver driver)
        {
            _driver = driver;
        }

        public void OpenPage()
        {
            _driver.Navigate().GoToUrl("https://cloud.google.com/");
        }

        public void SearchForPricingCalculator()
        {
            var searchIcon = _driver.FindElement(By.XPath("//input[@name='q']"));
            searchIcon.Click();
            searchIcon.SendKeys("Google Cloud Platform Pricing Calculator");
            searchIcon.SendKeys(Keys.Enter);
        }
    }

    public class GoogleCloudPricingCalculatorPage
    {
        private readonly IWebDriver _driver;

        public GoogleCloudPricingCalculatorPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public void ClickOnGoogleCloudPricingCalculator()
        {
            var pricingCalculatorLink = _driver.FindElement(By.XPath("//a[contains(text(), 'Google Cloud Platform Pricing Calculator')]"));
            pricingCalculatorLink.Click();
        }

        public void FillComputeEngineForm()
        {
            var computeEngineButton = _driver.FindElement(By.XPath("//md-tab-item//div[contains(text(), 'COMPUTE ENGINE')]"));
            computeEngineButton.Click();

            var instances = _driver.FindElement(By.Name("quantity"));
            instances.SendKeys("4");

            var operatingSystem = _driver.FindElement(By.Name("os"));
            operatingSystem.Click();
            _driver.FindElement(By.XPath("//md-option[@value='free']")).Click();

            var provisioningModel = _driver.FindElement(By.Name("provisioningModel"));
            provisioningModel.Click();
            _driver.FindElement(By.XPath("//md-option[@value='regular']")).Click();

            var machineFamily = _driver.FindElement(By.Name("family"));
            machineFamily.Click();
            _driver.FindElement(By.XPath("//md-option[@value='general']")).Click();

            var series = _driver.FindElement(By.Name("series"));
            series.Click();
            _driver.FindElement(By.XPath("//md-option[contains(text(), 'N1')]")).Click();

            var machineType = _driver.FindElement(By.Name("instanceType"));
            machineType.Click();
            _driver.FindElement(By.XPath("//md-option[contains(text(), 'n1-standard-8')]")).Click();

            var addGpuButton = _driver.FindElement(By.XPath("//md-checkbox[@aria-label='Add GPUs']"));
            addGpuButton.Click();

            var gpuType = _driver.FindElement(By.Name("gpuType"));
            gpuType.Click();
            _driver.FindElement(By.XPath("//md-option[contains(text(), 'NVIDIA Tesla V100')]")).Click();

            var gpuCount = _driver.FindElement(By.Name("gpuCount"));
            gpuCount.SendKeys("1");

            var localSsd = _driver.FindElement(By.Name("localSsd"));
            localSsd.Click();
            _driver.FindElement(By.XPath("//md-option[contains(text(), '2x375')]")).Click();

            var datacenter = _driver.FindElement(By.Name("location"));
            datacenter.Click();
            _driver.FindElement(By.XPath("//md-option[contains(text(), 'europe-west3')]")).Click();

            var committedUsage = _driver.FindElement(By.Name("commitmentTerm"));
            committedUsage.Click();
            _driver.FindElement(By.XPath("//md-option[contains(text(), '1 Year')]")).Click();

            var addToEstimateButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Add to Estimate')]"));
            addToEstimateButton.Click();
        }

        public string GetEstimatedCost()
        {
            var totalCostElement = _driver.FindElement(By.XPath("//div[@class='cpc-cart-total']/h2"));
            return totalCostElement.Text;
        }

        public void ShareEstimate()
        {
            var shareButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Share')]"));
            shareButton.Click();
        }

        public void OpenEstimateSummary()
        {
            var openSummaryButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Open estimate summary')]"));
            openSummaryButton.Click();
        }
    }

    [TestFixture]
    public class GoogleCloudPricingCalculatorTests
    {
        private IWebDriver _driver;
        private GoogleCloudPlatformHomePage _homePage;
        private GoogleCloudPricingCalculatorPage _calculatorPage;

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            _homePage = new GoogleCloudPlatformHomePage(_driver);
            _calculatorPage = new GoogleCloudPricingCalculatorPage(_driver);
        }

        [Test]
        public void TestPricingCalculator()
        {
            _homePage.OpenPage();
            _homePage.SearchForPricingCalculator();

            _calculatorPage.ClickOnGoogleCloudPricingCalculator();

            _calculatorPage.FillComputeEngineForm();
            var estimatedCost = _calculatorPage.GetEstimatedCost();

            Console.WriteLine($"Estimated Cost: {estimatedCost}");

            _calculatorPage.ShareEstimate();
            _calculatorPage.OpenEstimateSummary();

        }

        [TearDown]
        public void Teardown()
        {
            _driver.Quit();
        }
    }
}
